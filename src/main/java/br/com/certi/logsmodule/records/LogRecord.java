package br.com.certi.logsmodule.records;

import br.com.certi.logsmodule.entities.LogEntity;

import java.sql.Timestamp;

public record LogRecord(Long id, String action, Timestamp actionDate, String caller, String payLoad) {

    public LogRecord(LogEntity log) {
        this(log.getId(), log.getAction(), log.getActionDate(), log.getCaller(), log.getPayLoad());
    }
}
