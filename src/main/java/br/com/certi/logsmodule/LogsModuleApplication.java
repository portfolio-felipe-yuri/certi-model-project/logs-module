package br.com.certi.logsmodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogsModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogsModuleApplication.class, args);
	}

}
