package br.com.certi.logsmodule.repositories;

import br.com.certi.logsmodule.entities.LogEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<LogEntity, Long> {
}
