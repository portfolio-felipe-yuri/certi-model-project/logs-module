package br.com.certi.logsmodule.exceptions;

import java.io.Serial;

public class InvalidPassException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 4457633000513582694L;

    public InvalidPassException() {
        super("Invalid Password!");
    }
}
