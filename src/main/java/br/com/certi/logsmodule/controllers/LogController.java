package br.com.certi.logsmodule.controllers;

import br.com.certi.logsmodule.entities.LogEntity;
import br.com.certi.logsmodule.records.LogRecord;
import br.com.certi.logsmodule.repositories.LogRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("logs")
public class LogController {

    private final LogRepository repository;

    public LogController(LogRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public Page<LogRecord> findAll(@PageableDefault(size = 10, sort = {"actionDate"}) Pageable pagination) {
        // TODO: Requer autenticação
        return repository.findAll(pagination).map(LogRecord::new);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<LogRecord> addLog(@RequestBody LogRecord body) {
        // TODO: Requer autenticação
        repository.save(new LogEntity(body));
        return ResponseEntity.ok().body(body);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LogRecord> findById(@PathVariable Long id) {
        // TODO: Requer autenticação
        Optional<LogEntity> log = repository.findById(id);
        return log.map(logEntity -> ResponseEntity.ok(new LogRecord(logEntity))).orElseGet(() -> ResponseEntity.notFound().build());
    }
}
