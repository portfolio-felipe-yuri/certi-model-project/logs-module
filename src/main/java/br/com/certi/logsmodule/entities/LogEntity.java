package br.com.certi.logsmodule.entities;

import br.com.certi.logsmodule.records.LogRecord;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Table(name = "logs")
@Entity(name = "Logs")
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LogEntity {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String action;
    private Timestamp actionDate;
    private String caller;
    private String payLoad;

    public LogEntity(LogRecord log) {
        this.action = log.action();
        this.actionDate = log.actionDate();
        this.caller = log.caller();
        this.payLoad = log.payLoad();
    }
}
