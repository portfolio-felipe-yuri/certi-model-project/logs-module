ARG DOCKER_REGISTRY

FROM $DOCKER_REGISTRY/certi-devops-image

#Copy API artifact
COPY target/*.jar /opt/apps/

# Expose port logs API
EXPOSE 3000

# Run script
ENTRYPOINT [ "bash", "java -jar /opt/apps/*.jar" ]

